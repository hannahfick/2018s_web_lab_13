package ictgradschool.web.lab13.ex1;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }


        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");
            //Anything that requires the use of the connection should be in here...

            while (true) {

                System.out.println("Input a partial title here: ");
                Scanner input = new Scanner(System.in);
                String text = input.nextLine();



//                to quit the program here.
                if (text.equalsIgnoreCase("q")) { break; }

                try (PreparedStatement stmt = conn.prepareStatement("SELECT  * from lab13_articles WHERE title LIKE ?;")) {

                    stmt.setString(1, "%" + text + "%");

                    try (ResultSet result = stmt.executeQuery()) {

                        while (result.next()) {
                            String a = result.getString(result.findColumn("body"));
                            System.out.println(a);


                        }
                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
