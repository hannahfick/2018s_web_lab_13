package ictgradschool.web.lab13.ex2;


import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;


public class MovieDAO {


    public Movie getInfoByTitle(String title) {
        Properties dbProps = new Properties();
        Movie movie = new Movie();
        List<Person> peopleInvolved = new ArrayList<>();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            try (PreparedStatement stmt = conn.prepareStatement("SELECT f.film_title AS title, f" +
                    ".genre_name AS genre, concat(a.actor_fname, ' ', a.actor_lname) AS name, r.role_name AS role\n" +
                    "FROM pfilms_film AS f, pfilms_actor AS a, pfilms_role as r, pfilms_participates_in AS p\n" +
                    "WHERE f.film_title = ? AND f.film_id = p.film_id AND p.actor_id = a.actor_id" +
                    " AND p.role_id = r.role_id")) {

                stmt.setString(1, title);
                try (ResultSet r = stmt.executeQuery()) {

                    //get the properly formatted title and genre only once
                    if (r.next()){
                        movie.setTitle(r.getString(1));
                        movie.setGenre(r.getString(2));
                        r.previous();
                    }

                    //loop to add as many people as necessary
                    while (r.next()){
                        Person p = new Person();
                        p.setName(r.getString(3));

                        Role role = new Role();

                        role.setFilmTitle(movie);
                        role.setRoleName(r.getString(4));

                        List<Role> prevRoles = p.getMovieRoles();
                        prevRoles.add(role);
                        p.setMovieRoles(prevRoles);

                        peopleInvolved.add(p);
                    }
                    movie.setPeople(peopleInvolved);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return movie;
    }



    public List<Movie> getMoviesbyGenre(String searchGenre) {
        Properties dbProps = new Properties();
        List<Movie> movies = new ArrayList<>();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            try (PreparedStatement s = conn.prepareStatement("SELECT f.film_title AS title, f.genre_name AS genre FROM pfilms_film AS f WHERE f.genre_name = ?")) {
                (s).setString(1, searchGenre.toLowerCase());
                try (ResultSet r = (s).executeQuery()) {
                    while (r.next()) {
                        Movie m = new Movie();
                        m.setTitle(r.getString(1));
                        m.setGenre(r.getString(2));

                        movies.add(m);
                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return movies;
    }
}
