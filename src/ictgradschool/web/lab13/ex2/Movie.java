package ictgradschool.web.lab13.ex2;

import java.util.List;

public class Movie {

    private String title;
    private String genre;
    private List<Person> people;

    public Movie() {
    }

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
