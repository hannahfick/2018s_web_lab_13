package ictgradschool.web.lab13.ex2;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Properties;

public class PersonDAO {


    public Person getMoviesByActor(String searchString) {

        Person actor = new Person();

        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            // do all the network stuff
            //Anything that requires the use of the connection should be in here...

            try (PreparedStatement stmt = conn.prepareStatement(
                    "SELECT concat(a.actor_fname, ' ', a.actor_lname) as name, f.film_title, roles.role_name FROM pfilms_film as f, pfilms_role as roles, pfilms_participates_in as p, pfilms_actor as a WHERE (a.actor_fname LIKE ? OR a.actor_lname LIKE ?) AND p.actor_id = a.actor_id AND p.film_id = f.film_id AND p.role_id = roles.role_id")) {

                // want to search for names including the given string, in first and last
                searchString = "%" + searchString.trim() + "%";
                stmt.setString(1, searchString);
                stmt.setString(2, searchString);

                try (ResultSet r = stmt.executeQuery()) {

                    //getting the name part out of the while loop
                    if (r.next()) {
                        actor.setName(r.getString(1));
                        r.previous();
                    }

                    while (r.next()) {

                        Movie m = new Movie();

                        m.setTitle(r.getString(2));

                        //add the returned movies to the person's movie list
                        List<Movie> prevMovies = actor.getMovieTitles();
                        prevMovies.add(m);
                        actor.setMovieTitles(prevMovies);

                        // add the returned role string as a role obj to the person's roles list
                        Role role = new Role();

                        role.setFilmTitle(m);
                        role.setRoleName(r.getString(3));

                        List<Role> prevRoles = actor.getMovieRoles();
                        prevRoles.add(role);
                        actor.setMovieRoles(prevRoles);

                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return actor;
    }

}
