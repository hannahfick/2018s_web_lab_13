package ictgradschool.web.lab13.ex2;

import ictgradschool.web.Keyboard;

import java.util.List;
public class Exercise02 {

    final int ACTOR = 1;
    final int TITLE = 2;
    final int GENRE = 3;
    final int QUIT = 4;


    public void start() {

        System.out.println("Welcome to the Film database!\n");
        int choice = 0;
        while (choice == 0) {
            choice = getChoice();

            switch (choice) {
                case ACTOR:
                    search("actor");
                    choice = 0;
                    break;
                case TITLE:
                    search("title");
                    choice = 0;
                    break;
                case GENRE:
                    search("genre");
                    choice = 0;
                    break;
                case QUIT:
                    break;
            }
        }
    }

    public void search(String type) {
        boolean done = false;
        while (!done) {
            System.out.print("\nPlease enter the name of the " + type + " you wish to get information" +
                    " " +
                    "about, or press enter to return to the previous menu\n\n" +
                    "> ");
            String searchString = Keyboard.readInput();
            if (searchString.equals("")) {
                return;
            }

            switch (type) {
                case "actor":
                    doActorSearch(searchString);
                    break;
                case "title":
                    doTitleSearch(searchString);
                    break;
                case "genre":
                    doGenreSearch(searchString);
                    break;
            }
        }

    }


    public void doActorSearch(String actorNameFragment) {

        Person actor = new PersonDAO().getMoviesByActor(actorNameFragment);
        String actorFullName = actor.getName();

        if (actorFullName == null) {
            System.out.println("\nSorry, we couldn't find any actor by that name.\n");
        } else {

            System.out.println("\n" + actorFullName + " is listed as being involved in the " +
                    "following films: \n");

            for (Role role : actor.getMovieRoles()) {
                System.out.println(role.getFilm().getTitle() + " (" + role.getRoleName() + ")");
            }
        }

    }

    public void doTitleSearch(String searchTitle) {

        Movie film = new MovieDAO().getInfoByTitle(searchTitle);
        String title = film.getTitle();

        if (title == null) {
            System.out.println("\nSorry, we couldn't find any film by that name.\n");
        } else {

            System.out.println("\n" + title + " is a " + film.getGenre() + " movie that " +
                    "features the" +
                    " following people: \n");

            for (Person person : film.getPeople()) {

                List<Role> allroles = person.getMovieRoles();
                for (Role role : allroles) {
                    if (role.getFilm() == film) {

                        System.out.println(person.getName() + " (" + role.getRoleName() + ")");
                    }
                }
            }
        }

    }

    public void doGenreSearch(String searchGenre) {

        List<Movie> moviesByGenre = new MovieDAO().getMoviesbyGenre(searchGenre);

        if (moviesByGenre.size() == 0) {
            System.out.println("\nNo movies found for genre " + searchGenre + "\n");
        } else {
            System.out.println("The " + searchGenre + " genre includes the following films: ");
            for (Movie movie : moviesByGenre) {
                System.out.println(movie.getTitle());
            }
        }
    }

    public int getChoice() {

        System.out.println("Please select an option from the following:");
        System.out.println("1. Information by Person");
        System.out.println("2. Information by Movie");
        System.out.println("3. Information by Genre");
        System.out.println("4. Exit\n");
        System.out.print("> ");

        int option = 0;
        while (option == 0) {
            try {
                option = Integer.parseInt(Keyboard.readInput());
            } catch (NumberFormatException n) {
                option = 0;
            }
            if (1 > option || option > 4) {
                System.out.println("Please select a valid option: ");
                option = 0;
            }
        }
        return option;
    }


    public static void main(String[] args) {
        new Exercise02().start();
    }

}
