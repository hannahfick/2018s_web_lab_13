package ictgradschool.web.lab13.ex2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Person {
    private String name;

    private List<Movie> movieTitles = new ArrayList<>();


    private List<Role> movieRoles = new ArrayList<>();

    public int getActorID() {
        return actorID;
    }

    public void setActorID(int actorID) {
        this.actorID = actorID;
    }

    private int actorID;

    public Person() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public List<Movie> getMovieTitles() {
        return movieTitles;
    }

    public void setMovieTitles(List<Movie> movieTitles) {
        this.movieTitles = movieTitles;
    }

    public List<Role> getMovieRoles() {
        return movieRoles;
    }

    public void setMovieRoles(List<Role> movieRoles) {
        this.movieRoles = movieRoles;
    }
}
